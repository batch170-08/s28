fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));


async function fetchData() {
let result = await fetch("https://jsonplaceholder.typicode.com/todos")
let json = await result.json();
let data = json.map(function({title}){
	return {title}
	})
console.log(data);
}

fetchData();


async function fetchOneData() {
let result = await fetch("https://jsonplaceholder.typicode.com/todos/1")
let json = await result.json();
let statement = (`The item "${json.title}" on the list has a status of ${json.completed}`);
console.log(json);
console.log(statement)
}

fetchOneData();


fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "Created To Do List Item",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "Updated To Do List Item",
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "04/21/22",
		status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
});